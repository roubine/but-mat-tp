FROM gricad-registry.univ-grenoble-alpes.fr/roubine/docker/binder:cf3ab6d6

USER root
RUN userdel ${USER}

ARG NB_USER=joyvan
ARG NB_UID=1000
ENV USER ${NB_USER}
ENV NB_UID ${UID}
ENV HOME /home/${NB_USER}

RUN adduser --disabled-password \
    --gecos "Default user" \
    --uid ${NB_UID} \
    ${NB_USER}

WORKDIR ${HOME}
COPY . .
COPY .jupyter .jupyter
RUN chown -R ${NB_UID} ${HOME}
USER ${NB_USER}

RUN pip install -U pip \
    pip install -r requirements.txt && \
    jupyter contrib nbextension install --user && \
    jupyter nbextensions_configurator enable --user
