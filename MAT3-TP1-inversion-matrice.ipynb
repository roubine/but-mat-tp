{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0b5b7a93",
   "metadata": {},
   "source": [
    "# MAT3 - TP1: Inversion de matrices par la méthode des comatrices\n",
    "*L'inversion de matrices permet, entre autre, de simplifier la résolution de systèmes d'équations linéaires. Dans ce TP vous décrouvrirez les bases théoriques de la résolution ce genre de système à l'aide de matrices, vous développerez l'algorithme d'inversion de matrices en `python` puis, après avoir vérifié sa validité vous l'utiliserez afin de résoudre plusieurs systèmes.*"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7b4cda93",
   "metadata": {},
   "source": [
    "## Bases théoriques\n",
    "> ⚠️ **Conventions**\n",
    ">\n",
    "> - <u>LiCo :</u> En `python`, la notation indicielle correspond également à la convention LiCo. Le premier indice correspond au numéro de ligne et le deuxième au numéro de colonne. \n",
    "> - <u>Indices :</u> Afin de clarifier les notations, les indices commenceront ici par $0$ (et non $1$ comme dans le cours), afin de coller avec les conventions `python`.\n",
    "> - <u>Notations matrices/vecteurs :</u> Les matrices sont soulignées deux fois ($\\underline{\\underline{A}}$), les vecteurs sont soulignés une fois ($\\underline{x}$) et les scalaires ne sont pas soulignés."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "88411c57",
   "metadata": {},
   "source": [
    "### Mise en forme matricielle d'un système d'équation linéaires\n",
    "Un système d'équations linéaires se met généralement sous la forme (ici pour un système de 3 équations à 3 inconnues):\n",
    "\n",
    "$$\n",
    "\\begin{cases}\n",
    "    a_{00}\\ x_0 & + & a_{01}\\ x_1 & + & a_{02}\\ x_2 & = & b_0 \\\\ \n",
    "    a_{10}\\ x_0 & + & a_{11}\\ x_1 & + & a_{12}\\ x_2 & = & b_1 \\\\ \n",
    "    a_{20}\\ x_0 & + & a_{21}\\ x_1 & + & a_{22}\\ x_2 & = & b_2 \n",
    "\\end{cases}\n",
    "$$\n",
    "\n",
    "où $x_0$, $x_1$ et $x_2$ sont les inconnues à déterminer et les $a_{ij}$ et $b_i$ sont des coefficients connus.\n",
    "\n",
    "Il est possible de mettre ce système sous la forme matricielle suivante:\n",
    "\n",
    "$$\n",
    "\\underline{\\underline{A}}\\ \\underline{x} = \\underline{b}\n",
    "$$\n",
    "\n",
    "où\n",
    "\n",
    "$$\n",
    "\\underline{\\underline{A}} = \\left[a_{ij}\\right] = \n",
    "\\left[\\begin{array}{ccc}\n",
    "    a_{00} & a_{01} & a_{02} \\\\\n",
    "    a_{10} & a_{11} & a_{12} \\\\\n",
    "    a_{20} & a_{21} & a_{22}\n",
    "\\end{array}\\right], \\quad\n",
    "\\underline{x} =\n",
    "\\left[\\begin{array}{c}\n",
    "    x_0 \\\\\n",
    "    x_1 \\\\\n",
    "    x_2\n",
    "\\end{array}\\right] \\quad \\text{et} \\quad\n",
    "\\underline{b} =\n",
    "\\left[\\begin{array}{c}\n",
    "    b_0 \\\\\n",
    "    b_1 \\\\\n",
    "    b_2\n",
    "\\end{array}\\right]\n",
    "$$\n",
    "\n",
    "sont respectivement:\n",
    "- la matrice carrée des coefficients $a_{ij}$ (connus) de taille $3$,\n",
    "- le vecteur des inconnus $x_i$ de taille $3$ et\n",
    "- le vecteur des coefficients $b_i$ (connus) de taille $3$.\n",
    "\n",
    "> ✍️ Vérifier à la main que les deux écritures sont équivalentes.\n",
    "\n",
    "Cette notation peut s'étendre à des systèmes de $n$ équations à $n$ inconnues. Cela fera intervenir des matrices carrées et des vecteurs de taille $n$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "43410160",
   "metadata": {},
   "source": [
    "### Inversion de matrice\n",
    "L'inversion d'une matrice est une opération qui s'effectue sur une matrice carrée et produit une matrice carrée de même taille.\n",
    "L'inverse de la matrice $\\underline{\\underline{A}}$ se note $\\underline{\\underline{A}}^{-1}$.\n",
    "\n",
    "L'inversion confère la propriété suivante à $\\underline{\\underline{A}}$ et $\\underline{\\underline{A}}^{-1}$:\n",
    "$$\n",
    "\\underline{\\underline{A}}\\ \\underline{\\underline{A}}^{-1} = \\underline{\\underline{A}}^{-1}\\underline{\\underline{A}} = \\mathbb{1}\n",
    "$$\n",
    "\n",
    "Une matrice est inversible si son déterminant est non nul."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5d55673b",
   "metadata": {},
   "source": [
    "### Utilisation de la matrice inverse pour résoudre un système\n",
    "Sous réserve de pouvoir calculer l'inverse de la matrice $\\underline{\\underline{A}}$, le système ci-dessus peut se résoudre comme suit:\n",
    "\n",
    "$$\n",
    "\\begin{array}{lrcl}\n",
    "    & \\underline{\\underline{A}}\\ \\underline{x} & = &  \\underline{b} \\\\\n",
    "    \\Leftrightarrow & \\underbrace{\\underline{\\underline{A}}^{-1} \\underline{\\underline{A}}}_{\\mathbb{1}}\\ \\underline{x} & = & \\underline{\\underline{A}}^{-1} \\underline{b} \\quad \\textit{(on multiplie à gauche par $\\underline{\\underline{A}}^{-1}$ des deux côtés)}\\\\\n",
    "        \\Leftrightarrow & \\underline{x} & = & \\underline{\\underline{A}}^{-1} \\underline{b}\n",
    "\\end{array}\n",
    "$$\n",
    "\n",
    "---\n",
    "**RÉSULTAT**\n",
    "\n",
    "Le vecteur des inconnues $\\underline{x}$ est directement déterminé en multipliant le vecteur $\\underline{b}$ par l'inverse de la matrice $\\underline{\\underline{A}}$.\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "94b11f46",
   "metadata": {},
   "source": [
    "## Utilisation des matrices en  `python`\n",
    "\n",
    "### Fonctionnalités de base de `numpy`\n",
    "La librairie `numpy` offre beaucoup d'outils pour faire des opérations sur les matrices.\n",
    "\n",
    "> ✍️ Éxecuter et modifer les cellules suivantes afin de comprendre l'utilisation des fonctions qui vous seront utiles pour la suite. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2575a395",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy\n",
    "\n",
    "# définition d'une matrice: numpy.array([...])\n",
    "A = numpy.array(\n",
    "    [\n",
    "        [1, 1, 1],  # ligne 1\n",
    "        [-2, 7, 4],  # ligne 2\n",
    "        [0, -5, 4],  # ligne 3\n",
    "    ]\n",
    ")\n",
    "print('Matrice A')\n",
    "print(A)\n",
    "\n",
    "# Lecture de la ligne i et de la colonne j de la matrice\n",
    "i = 2\n",
    "j = 1\n",
    "print('Indice de la ligne: i =', i)\n",
    "print('Indice de la colonne j =', j)\n",
    "print('Element i, j de A: a_ij =', A[i, j])\n",
    "\n",
    "# Modification de la ligne i et de la colonne j de la matrice\n",
    "A[i, j] = -6\n",
    "print('Nouvelle valeur de a_ij =', A[i, j])\n",
    "print('Nouvelle valeur de A')\n",
    "print(A)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "75ebf3d7",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Taille de la matrice: A.shape (tableau de taille 2 donnant le nombre de ligne et de colonnes de A)\n",
    "n, m = A.shape\n",
    "print('Nombre lignes: n =', n)\n",
    "print('Nombre colonnes: m =', m)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "62366b8d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Déterminant d'une matrice: numpy.linalg.det(A)\n",
    "detA = numpy.linalg.det(A)\n",
    "print('det(A) =', detA)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a0bdd8d5",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Inverse d'une matrice: numpy.linalg.inv(A)\n",
    "invA = numpy.linalg.inv(A)\n",
    "print('Inverse de A: inv(A)')\n",
    "print(invA)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "22ffc8be",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Multiplication de deux matrices: numpy.matmul(A, B)\n",
    "Id = numpy.matmul(invA, A)\n",
    "print('Vérification que invA x A = Id')\n",
    "print(Id)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13b0f5af",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Suppression d'une ligne: numpy.delete(A, i, 0)\n",
    "# A: la matrice dont on veut supprimer la ligne\n",
    "# i: la ième ligne (attention on commence à 0)\n",
    "# 0: pour ligne (1 pour supprimer une colonne)\n",
    "i = 2\n",
    "subA = numpy.delete(A, i, 0)  # suppression de la troisième ligne\n",
    "print('A sans la ligne', i)\n",
    "print(subA)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a68bc66e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Suppression d'une colonne: numpy.delete(A, j, 1)\n",
    "# A: la matrice dont on veut supprimer la colonne\n",
    "# j: la jème colonne (attention on commence à 0)\n",
    "# 1: pour colonne (0 pour supprimer une ligne)\n",
    "j = 1\n",
    "subA = numpy.delete(A, j, 1)  # suppression de la deuxième colonne\n",
    "print('A sans la colonne', j)\n",
    "print(subA)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2c4f74fc",
   "metadata": {},
   "source": [
    "### Exercice d'application direct\n",
    "> ✍️ À l'aide de la fonction `numpy.linalg.inv()`, résoudre le système suivant en `python`:\n",
    "\n",
    "$$\n",
    "\\begin{cases}\n",
    "    x_1 & + & x_2 & + & x_3 & = & 1 \\\\ \n",
    "    x_1 & + & 5x_2 & + & 4x_3 & = & -1 \\\\ \n",
    "    x_1 & + & 5x_2 & + & 14x_3 & = & 2\n",
    "\\end{cases}\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cbb0ed69",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Effacer la mémoire avant de continuer\n",
    "%reset -f\n",
    "# Import de librairies python\n",
    "import numpy\n",
    "\n",
    "# Étape 1: Définition de A\n",
    "A = numpy.array([...])\n",
    "# Étape 2: Définition de b\n",
    "b = numpy.array([...])\n",
    "# Étape 3: Calcul de l'inverse de A (avec numpy)\n",
    "...\n",
    "# Étape 4: Calcul de x\n",
    "...\n",
    "# Étape 5: vérification de la validité de l'inverse de A\n",
    "...\n",
    "# Étape 6: vérification de la validité de x\n",
    "..."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6071bd98",
   "metadata": {},
   "source": [
    "## Inversion de matrices par la méthode des comatrices\n",
    "Le but principal du TP est de coder l'algorithme d'inversion de matrice afin de remplacer la boîte noire `numpy.linalg.inv()`.\n",
    "\n",
    "Il existe plusieurs méthodes pour inverser une matrice. Ici nous allons coder celle qui utilise les _comatrices_.\n",
    "\n",
    "### Calcul de la comatrice\n",
    "La comatrice de $\\underline{\\underline{A}}$ notée $\\underline{\\underline{C}}$ est une matrice carrée de même taille $n$ que $\\underline{\\underline{A}}$ définie par:\n",
    "\n",
    "$$\n",
    "\\underline{\\underline{C}} = \\left[c_{ij}\\right] \\quad \\text{avec} \\quad c_{ij} = (-1)^{i+j} \\ \\text{det}\\left(\\underline{\\underline{s}}^{(ij)}\\right)\n",
    "$$\n",
    "\n",
    "où $\\underline{\\underline{s}}^{(ij)}$ est une sous matrice carrée de taille $n-1$ déduite de $\\underline{\\underline{A}}$ en supprimant le $i$-ème ligne et $j$-ème colonne.\n",
    "\n",
    "> ✍️ Calculer les coefficients $c_{00}$ et $c_{12}$ de la comatrice de la matrice \n",
    "$$\n",
    "    \\underline{\\underline{A}} = \n",
    "    \\left[\\begin{array}{ccc}\n",
    "        1 & 1 & 1 \\\\ \n",
    "        1 & 5 & 4 \\\\ \n",
    "        1 & 5 & 14\n",
    "    \\end{array}\\right]\n",
    "$$\n",
    "> à la main.\n",
    "\n",
    "### Calcul de l'inverse\n",
    "Une fois la comatrice calculée, l'inverse de $\\underline{\\underline{A}}$ se déduit simplement par:\n",
    "\n",
    "$$\n",
    "\\underline{\\underline{A}}^{-1} = \\frac{1}{\\text{det}\\left(\\underline{\\underline{A}}\\right)}\\ \\underline{\\underline{C}}^\\text{T}\n",
    "$$\n",
    "\n",
    "> ✍️ Implémentation de l'algorithme:\n",
    "> 1. Compléter les cellules ci-dessous afin de calculer l'inverse de la matrice définie plus haut puis, connaissant les propriétés de l'inverse d'une matrice, vérifier que votre fonction est correctement implémentée.\n",
    "> 2. Executer votre fonction avec $\\underline{\\underline{A}}$ de taille $n = 5$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "758f29d2-4f4d-4a34-ae7d-4817e815bc33",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Effacer la mémoire avant de continuer\n",
    "%reset -f\n",
    "# Import de librairies python\n",
    "import numpy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fdd646a0",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Définition de la fonction qui inverse une matrice\n",
    "def inversion_matrice(A):\n",
    "    \"\"\" Fonction qui retourne l'inverse de la matrice A en utilisant la méthode de la comatrice\n",
    "    \"\"\"\n",
    "    # Étape 1: calcul de la taille de A et vérification que A est carrée\n",
    "    # n, m = ...\n",
    "    #print('Nombre de lignes de A: n =', n)\n",
    "    #print('Nombre de colonnes de A: m =', m)\n",
    "    \n",
    "    # Étape 2: calcul de la comatrice C\n",
    "    # initialisation de la comatrice C (taille n par n remplie de 0)\n",
    "    # C = numpy.zeros((n, n))  \n",
    "    # double boucle imbriquée sur les indices i et j\n",
    "    # for i in range ...\n",
    "\n",
    "    # Étape 3: calcul du déterminant de A\n",
    "    # detA = ...\n",
    "\n",
    "    # Étape 4: calcul de l'inverse de A\n",
    "    invA = ...\n",
    "\n",
    "    return invA"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dafade5d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Définition de A\n",
    "A = numpy.array([...])\n",
    "\n",
    "# Appel de la fonction inversion_matrice(A)\n",
    "invA = inversion_matrice(A)\n",
    "\n",
    "# Verification invA x A = Id\n",
    "..."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "85bc5057-94bd-4140-9082-1343a6d65bda",
   "metadata": {},
   "source": [
    "### Performances \n",
    "Dans cette partie nous allons tester les performances de l'algorithme de comatrice. Pour ce faire nous utiliserons le module `numpy.random` pour générer des matrices de taille `n` et `time` pour mesurer le temps d'execution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1a19f998-e278-476c-8c09-c1afe656cde3",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy\n",
    "import time\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# définition d'une matrice aléatoire de taille n\n",
    "n = 10\n",
    "A = numpy.random.rand(n, n)\n",
    "\n",
    "# appel à votre fonction inversion_matrice\n",
    "t1 = time.thread_time()\n",
    "invA = inversion_matrice(A)\n",
    "t2 = time.thread_time()\n",
    "print(\"temps d'execution: \", t2 - t1, \"secondes\") "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5ede25a1-35bd-43c2-8464-ccd17dd08431",
   "metadata": {},
   "source": [
    "> ✍️ Tracer sur un graph deux courbes du temps d'execution en fonction de la taille de la matrice pour des tailles allant de $10$ à $150$ (en utilisant `numpy.arange`) de $10$ en $10$ pour votre fonction `inversion_matrice` et `numpy.linalg.inv`.\n",
    "> Quelles sont vos conclusions ?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ff53e0d6-0ca5-4a94-b26c-b45a50deed4e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Calcul du temps des tailles\n",
    "tailles = []\n",
    "\n",
    "# boucles sur les tailles qui enregistre les temps pour la fonction numpy\n",
    "temps_numpy = []\n",
    "# for n in tailles:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cbef33ec-cda1-449d-9da7-552bfce18e60",
   "metadata": {},
   "outputs": [],
   "source": [
    "# boucles sur les tailles qui enregistre les temps pour l'algorithme des comatrices\n",
    "temps_comatrice = []\n",
    "# for n in tailles:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2c6064f8-abda-48ec-9a99-444f92fe9706",
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot\n",
    "plt.plot(tailles, temps_comatrice, label=\"comatrice\")\n",
    "plt.plot(tailles, temps_numpy, label=\"numpy\")\n",
    "plt.yscale(\"log\")\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ec4111ba-9f69-4d44-b5a0-911dc0ae2e40",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.2"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
