import math
from IPython.display import display, Math

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import numpy as np

class Vecteur:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def __getitem__(self, index):
        if index > 2:
            raise ValueError(f"index should be between 0 and 2. {index} given")
        return [self.x, self.y, self.z][index]

    def __neg__(self):
        return Vecteur(-self.x, -self.y, -self.z)
    
    def __add__(self, other):
        return Vecteur(self.x + other.x, self.y + other.y, self.z + other.z)

    def __sub__(self, other):
        return Vecteur(self.x - other.x, self.y - other.y, self.z - other.z)

    def __mul__(self, other):
        if isinstance(other, Vecteur):
            return (self.x * other.x + self.y * other.y + self.z * other.z)
        else:
            return Vecteur(other * self.x, other * self.y, other * self.z)

    def __truediv__(self, other):
        if isinstance(other, Vecteur):
            raise ValueError("Can't devide by Vecteur")
        return Vecteur(self.x / other, self.y / other, self.z / other)

    def __floordiv__(self, other):
        if isinstance(other, Vecteur):
            raise ValueError("Can't devide by Vecteur")
        return Vecteur(self.x // other, self.y // other, self.z // other)
    
    def __rmul__(self, other):
        return self.__mul__(other)
    
    def __xor__(self, other):
        retx = self.y * other.z - self.z * other.y
        rety = self.z * other.x - self.x * other.z
        retz = self.x * other.y - self.y * other.x
        return Vecteur(retx, rety, retz)
    
    def norme(self):
        return math.sqrt(self.x * self.x + self.y * self.y + self.z * self.z)
    
    def normer(self):
        n = self.norme()
        self.x = self.x / n
        self.y = self.y / n
        self.z = self.z / n 
        
    def __repr__(self):
        return f"Vecteur({self.x:.4f}, {self.y:.4f}, {self.z:.4f})"
    
    def __str__(self):
        return f"({self.x:.4f}, {self.y:.4f}, {self.z:.4f})"
    
class Droite:
    def __init__(self, point, vdir):
        self.point = point
        self.vdir = vdir
        
    def __repr__(self):
        return f"Droite d'équation paramétrique :\n"\
            f"x = {self.point.x} + k({self.vdir.x})\n"\
            f"y = {self.point.y} + k({self.vdir.y})\n"\
            f"z = {self.point.z} + k({self.vdir.z})"
    
    def __str__(self):
        return f"x = {self.point.x} + k({self.vdir.x})\n"\
            f"y = {self.point.y} + k({self.vdir.y})\n"\
            f"z = {self.point.z} + k({self.vdir.z})"
    
class Plan:
    def __init__(self, point, normal):
        self.point = point
        self.normal = normal     
        
    def __repr__(self):
        s = -self.point * self.normal
        return f"Plan d'équation cartésienne :\n"\
            f"({self.normal.x:.4f})x + ({self.normal.y:.4f})y + ({self.normal.z:.4f})z + ({s:.4f}) = 0"
    
    def __str__(self):
        s = -self.point * self.normal
        return f"({self.normal.x:.4f})x + ({self.normal.y:.4f})y + ({self.normal.z:.4f})z + ({s:.4f}) = 0"
    
def resoudre_system_lineaire_2eq2inc(a11, a12, a21, a22, b1, b2):
    y = (b2 - a12 * b1 / a11) / (a22 - a12 * a12 / a11)
    x = b1 / a11 - a12 / a11 * y
    return x, y  

def affiche_vecteur(nom, val):
    display(Math(r'\vec{' f'{nom}' r'} =' f' {val}'))

def affiche_norme(nom, val):
    display(Math(r'\Vert\vec{' f'{nom}' r'}\Vert =' f' {val:.4f}'))
    
def affiche_produit_scalaire(nom1, nom2, val):
    display(Math(r'\vec{' f'{nom1}' r'}\cdot\vec{' f'{nom2}' r'} = ' f'{val:.4f}'))
    
def affiche_produit_vectoriel(nom1, nom2, val):
    display(Math(r'\vec{' f'{nom1}' r'}\wedge\vec{' f'{nom2}' r'} = ' f'{val}'))
    
    
def affiche_balle(nphi, ntheta, color_func, source):
    r = 1.0
    phi = np.linspace(0, 360, nphi) / 180.0 * np.pi
    theta = np.linspace(-90, 90, ntheta) / 180.0 * np.pi
    
    cols = []
    for i in range(len(phi) - 1):
        for j in range(len(theta) - 1):
            cols.append((0.0, 0.0, 0.0))

    verts2 = []
    for i in range(len(phi) - 1):
        for j in range(len(theta) - 1):
            cp0 = r * np.cos(phi[i])
            cp1 = r * np.cos(phi[i + 1])
            sp0 = r * np.sin(phi[i])
            sp1 = r * np.sin(phi[i + 1])

            ct0 = np.cos(theta[j])
            ct1 = np.cos(theta[j + 1])

            st0 = r * np.sin(theta[j])
            st1 = r * np.sin(theta[j + 1])

            verts = []
            verts.append((cp0 * ct0, sp0 * ct0, st0))
            verts.append((cp1 * ct0, sp1 * ct0, st0))
            verts.append((cp1 * ct1, sp1 * ct1, st1))
            verts.append((cp0 * ct1, sp0 * ct1, st1))
            verts2.append(verts)
            
    # couleurs
    nf = len(verts2)
    orig = Vecteur(0.0, 0.0, 0.0)
    rayon = source - orig
    rayon /= rayon.norme()
    # rayon.normer()
    for f in range(nf):
        p1 = Vecteur(verts2[f][0][0], verts2[f][0][1], verts2[f][0][2])
        p2 = Vecteur(verts2[f][1][0], verts2[f][1][1], verts2[f][1][2])
        p3 = Vecteur(verts2[f][2][0], verts2[f][2][1], verts2[f][2][2])
        niveau_gris = color_func(p1, p2, p3, rayon) # c'est ici que la fonction des étudiants est appelée
        cols[f] = (niveau_gris, niveau_gris, niveau_gris)
        
    # dessin
    fig = plt.figure('SPLTV', figsize = (15, 8))
    custom=plt.subplot(121, projection = '3d')

    poly3= Poly3DCollection(verts2, facecolor = cols)  

    custom.add_collection3d(poly3)
    custom.set_xlabel('X')
    custom.set_xlim3d(-1, 1)
    custom.set_ylabel('Y')
    custom.set_ylim3d(-1, 1)
    custom.set_zlabel('Z')
    custom.set_zlim3d(-1, 1)
    plt.show()
