# BUT: TP des modules MAT

L'intégralité des TP se dérouleront sur la plateforme Jupyter de l'UGA qui est accessible via un navigateur internet à cette adresse: https://gricad-jupyter.univ-grenoble-alpes.fr.
Vous pourrez y accéder aussi bien de l'IUT que de chez vous.

**Pour commencer les TP, suivre les consignes ci-dessous.**

## Téléchargement des sujets de TP

Télécharger les sujets de TP sur la plateforme avec le lien ci-dessous.

[>> Télécharger les sujets de TP <<](https://gricad-jupyter.univ-grenoble-alpes.fr/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Froubine%2Fbut-mat-tp)

:warning: En cas de problème (Erreur 404 ou 500), suivre les consignes ci-dessous.

## :warning: En cas de problème
Si en cliquant sur le lien de téléchargement vous optenez une erreur 404 on 500, suivez les étapes suivantes:
* Sur votre ordinateur:
    * Télécharger le fichier [`MAT-setup.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/roubine/but-mat-tp/-/raw/master/MAT-setup.ipynb?inline=false).
* Sur l'interface [jupyter](https://gricad-jupyter.univ-grenoble-alpes.fr):
    * Créez un dossier spécifique pour les TP de math (en haut à droit: `New -> Folder`)
    * Uploader `MAT-setup.ipynb` dans ce dossier (bouton en haut à droit `upload`)
    * Exéctuer les étapes de l'étape 2.

