from matplotlib import pyplot as plt
import numpy
import random
plt.rcParams['figure.figsize'] = [10, 5]
plt.rcParams['font.size'] = 16
plt.rcParams['patch.force_edgecolor'] = True
myred = (1, 0, 0, 0.5)

# plot polygons
def draw_polygons(f, start, stop, step, label=None):
    """ This function plots polygons under the function f
    from `start` to `stop` with width `step`
    """
    i = start
    while i + step < stop:
        x = numpy.array([i, i + step])
        if label and i == start:
            plt.fill_between(x, f(x), fc=myred, lw=min(step, 1), label=label);
        else:
            plt.fill_between(x, f(x), fc=myred, lw=min(step, 1));
        i += step

    if i < stop:
        x = numpy.array([i, stop])
        plt.fill_between(x, f(x), fc=myred, lw=min(step, 1));

# plot numerical integration
def TP2_plot_numerical_integration(f, w=1, a=-1, b=1):
    plt.clf()

    # x/y axis
    plt.axhline(y=0, color='k', lw=1, alpha=0.3)
    plt.axvline(x=0, color='k', lw=1, alpha=0.3)

    # plot the blue curve
    x = numpy.arange(a - 1, b + 1, w / 100)
    plt.plot(x, f(x), lw=4, label="$y = f(x)$");

    # plot the red surface under the curve
    margin = 0.1 * (a - b)
    x = numpy.arange(a - margin, b + w + margin, w)
    draw_polygons(f, a, b, w, label=f'Trapèze $w={w}$')

    # text
    plt.legend();
    xl = list(numpy.arange(a, b + 1, 2))
    xl[0] = f'a = {a}'
    xl[-1] = f'b = {b}'
    plt.xticks(numpy.arange(a, b + 1, 2), xl)
    # plt.yticks([])
    plt.title('Méthode des trapèze');
    # plt.savefig(f'MAT2-TP2-polygons-{w}.png')

# plot integration image
def TP2_plot_integral():

    # define exemple function
    def f(x):
        return - x ** 2 + x ** 3 - 0.1 * x ** 4 - 3 * x + 40


    plt.clf()

    # x/y axis
    plt.axhline(y=0, color='k', lw=1, alpha=0.3)
    plt.axvline(x=0, color='k', lw=1, alpha=0.3)

    # plot the blue curve
    x = numpy.arange(-3, 9.1, 0.1)
    plt.plot(x, f(x), lw=4, label="$y = f(x)$");

    # plot the red surface under the curve
    x = numpy.arange(-2, 8.1, 0.1)
    plt.fill_between(x, f(x), color=myred, label="Aire$= G$");

    # plot balck boundaries
    a = x.min()
    b = x.max()
    plt.plot([a, a], [0, f(a)], color='k', lw=3, zorder=-1)
    plt.plot([b, b], [0, f(b)], color='k', lw=3, zorder=-1)
    plt.plot([a, b], [0, 0], color='k', lw=3, zorder=-1)

    # text
    plt.legend();
    plt.xlabel('$x$')
    plt.ylabel('$y$')
    plt.xticks([a, b], ['$a$', '$b$'])
    plt.yticks([])
    plt.title('Intégrale de $f(x)$ et aire sous la courbe');
    plt.text((a + b) / 2, (f((a + b) / 2) / 2),
             '$G = \int_a^b f(x) dx$ \n= Aire sous la courbe',
             va='center',
             ha='center');


# build data for exercice
def TP2_build_data():
    def f(x):
        y = 3 * numpy.exp(-(x - 5) ** 2 / 10) + x / 10 - 3 * numpy.exp(-(1 - 5) ** 2 / 10)
        y += 0.1 * (random.random() - 0.5)
        return max(0, y)

    for name, w in [('coarse', 1), ('fine', 0.1)]:
        with open(f'MAT2-TP2-profile-{name}.dat', 'w') as file:
            file.write(f'#x\ty\n')
            for x in numpy.arange(0, 11, w):
                if x == 8:
                    xf = x
                else:
                    xf = max(0, x + 0.5 * w * (random.random() - 0.5))
                file.write(f'{xf}\t{f(x)}\n')
